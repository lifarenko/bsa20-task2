﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Reflection;
using System.Text.RegularExpressions;
using CoolParking.BL.Models;
using CoolParking.BL.Services;

namespace CoolParking.CI
{
    class Program
    {

        static public TimerService withdrawTimer = new TimerService();
        static public TimerService logTimer = new TimerService();
        static public LogService logService = new LogService($@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Transactions.log");

        static public ParkingService parkingService = new ParkingService(withdrawTimer, logTimer, logService);


        private class MenuItem
        {
            public string Description { get; set; }
            public Action Execute { get; set; }
        }


        private static List<MenuItem> MenuItems;

        private static void PopulateMenuItems()
        {

            MenuItems = new List<MenuItem>
            {
              new MenuItem {Description = "Show Parking balance", Execute = ShowBalance},
              new MenuItem {Description = "Show earned money", Execute = EarnedMoney},
              new MenuItem {Description = "Show free places", Execute = FreePlaces},
              new MenuItem {Description = "Show Transactions", Execute = ShowTransaction},
              new MenuItem {Description = "Show history Transaction", Execute = ShowHistory},
              new MenuItem {Description = "Show vechicles", Execute = ShowVechicles},
              new MenuItem {Description = "Add vechicle", Execute = AddVechicle},
              new MenuItem {Description = "Remove vechicle", Execute = RemoveVechicle},
              new MenuItem {Description = "To up vechicle", Execute = ToUpVechicle},
              new MenuItem {Description="To end", Execute=End},
            };
        }

        private static void End()
        {
            ClearAndShowHeading("Exit");
            Console.WriteLine("Press any button");
        }

        private static void RemoveVechicle()
        {
            ClearAndShowHeading("Remove vechicle");
            Console.WriteLine("Please enter Id");
            string id = Console.ReadLine();
            string pattern = @"[A-Z]{2}-\d{4}-[A-Z]{2}";

            if (Regex.IsMatch(id, pattern))
            {
                parkingService.RemoveVehicle(id);
            }
            else
            {
                Console.WriteLine("Wrong id");
            }

            GoToMainMenu();
        }

        private static void ToUpVechicle()
        {
            ClearAndShowHeading("ToU up vechicle");
            Console.WriteLine("Please enter Id");
            string id = Console.ReadLine();
            string pattern = @"[A-Z]{2}-\d{4}-[A-Z]{2}";

            if (Regex.IsMatch(id, pattern))
            {
                Console.WriteLine("Please enter sum");
                int sum = 0;
                try
                {
                    sum = Int32.Parse(Console.ReadLine());
                    parkingService.TopUpVehicle(id, sum);
                }
                catch (FormatException)
                {
                    Console.WriteLine($"Unable to parse");
                }

            }
            else
            {
                Console.WriteLine("Wrong id");
            }

            GoToMainMenu();
        }


        private static void ShowVechicles()
        {
            ClearAndShowHeading("Show vechicles");
            ReadOnlyCollection<Vehicle> vechicles = parkingService.GetVehicles();
            foreach (var item in vechicles)
            {
                Console.WriteLine(item.Id + " " + item.VehicleType + " " + item.Balance);
            }

            GoToMainMenu();
        }


        private static void ShowTransaction()
        {
            ClearAndShowHeading("Show Transaction");
            TransactionInfo[] transactions = parkingService.GetLastParkingTransactions();
            foreach (var item in transactions)
            {
                Console.WriteLine(item.time + " " + item.Id + " " + item.Sum);
            }
            GoToMainMenu();
        }


        private static void ShowHistory()
        {
            ClearAndShowHeading("Show history transaction");
            Console.WriteLine(logService.Read());
            GoToMainMenu();
        }


        private static void AddVechicle()
        {
            ClearAndShowHeading("Add");


            Console.WriteLine("Please enter Id, VechicleType, Balance");
            try
            {
                string str = Console.ReadLine();
                VehicleType vehicleType;
                string[] mystring = str.Split(' ');

                switch (mystring[1])
                {
                    case "PassengerCar":
                        vehicleType = VehicleType.PassengerCar;
                        break;
                    case "Truck":
                        vehicleType = VehicleType.Truck;
                        break;
                    case "Bus":
                        vehicleType = VehicleType.Bus;
                        break;
                    case "Motorcycle":
                        vehicleType = VehicleType.Motorcycle;
                        break;
                    default:
                        throw new Exception();

                }
                Vehicle vehicle = new Vehicle(mystring[0], vehicleType, decimal.Parse(mystring[2]));
                parkingService.AddVehicle(vehicle);

                Console.WriteLine("Add");
            }
            catch
            {
                Console.WriteLine("Can't add");
            }

            GoToMainMenu();
        }

        private static void EarnedMoney()
        {
            ClearAndShowHeading("Earned Money");
            Console.WriteLine(parkingService.GetEarnedMoney());
            GoToMainMenu();
        }


        private static void FreePlaces()
        {
            ClearAndShowHeading("Free");
            Console.WriteLine(parkingService.GetFreePlaces());
            GoToMainMenu();
        }



        private static void ShowBalance()
        {
            ClearAndShowHeading("Parking Balance");
            Console.WriteLine(parkingService.GetBalance());
            GoToMainMenu();
        }


        private static void ClearAndShowHeading(string heading)
        {
            Console.Clear();
            Console.WriteLine(heading);
            Console.WriteLine(new string('-', heading?.Length ?? 0));
        }

        private static void GoToMainMenu()
        {
            Console.Write("\nPress any key to go to the main menu...");
            Console.ReadKey();
            MainMenu();
        }



        static void MainMenu()
        {
            
            Console.Clear();
            Console.WriteLine("Main Menu");
            Console.WriteLine("---------\n");

            for (int i = 0; i < MenuItems.Count; i++)
            {
                Console.WriteLine($"  {i + 1}. {MenuItems[i].Description}");
            }

            int cursorTop = Console.CursorTop + 1;
            int userInput;

            do
            {
                Console.SetCursorPosition(0, cursorTop);
                Console.Write(new string(' ', Console.WindowWidth));
                Console.SetCursorPosition(0, cursorTop);

                Console.Write($"Enter a choice (1 - {MenuItems.Count}): ");
            } while (!int.TryParse(Console.ReadLine(), out userInput) ||
                        userInput < 1 || userInput > MenuItems.Count );

     
                MenuItems[userInput - 1].Execute();
        }





        static void Main(string[] args)
        {
            try
            {
                PopulateMenuItems();
                MainMenu();

                Console.ReadKey();
            }
            finally
            {
                File.Delete($@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Transactions.log");
            }
            
        }
    }
}
