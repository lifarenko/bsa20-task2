﻿// TODO: implement the LogService class from the ILogService interface.
//       One explicit requirement - for the read method, if the file is not found, an InvalidOperationException should be thrown
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in LogServiceTests you can find the necessary constructor format.

using System;
using CoolParking.BL.Interfaces;
using System.IO;
using System.Reflection;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        public LogService(string LogPath)
        {
            string pattern = @".*Transactions\.log$";
            if (Regex.IsMatch(LogPath, pattern))
            //if (LogPath == $@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Transactions.log")
            {
                this.LogPath = LogPath;
            }
            else
            {
                throw new ArgumentException();
            }
        }
        public string LogPath { get; }

        public string Read()
        {
            string actual;
            if (File.Exists(LogPath))
            {
               using (var file = new StreamReader(LogPath))
              {
                actual = file.ReadToEnd();
              }
            }
            else
            {
               throw new InvalidOperationException();
            }
            return actual;
        }

        public void Write(string logInfo)
        {
            using (var file = new StreamWriter(LogPath, true))
            {
                file.WriteLine(logInfo);
            }
        }
        public void Dispose()
        {
            File.Delete(LogPath);
        }
    }
}